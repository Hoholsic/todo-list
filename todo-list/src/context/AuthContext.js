import {createContext} from 'react';

function noop () {};

export const AuthContext = createContext({
	token: null,
	user: null,
	avatar: null,
	tasks: null,
	task: null,
	getAllTask: noop,
	me: noop,
	setTask: noop,
	login: noop,
	setAvatar: noop,
	logout: noop,
	isAuthenticated: false,
	isLoaded: false
});