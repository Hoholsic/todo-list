import { useState, useCallback, useEffect, useMemo } from 'react';
import { AuthContext } from "../context/AuthContext";
import {useHttp} from '../hooks/http.hook';
import {USER} from '../api/endpoints';

const storageName = 'userData';

export const AuthProvider = (props) => {
	const [isLoaded, setIsLoaded] = useState(false);
	const [token, setToken] = useState(null);
	const [user, setUser] = useState(null);
	const [avatar, setAvatar] = useState(null);
	const [tasks, setTasks] = useState([]);
	const [task, setTask] = useState(null);
	const {request} = useHttp();

	const login = useCallback((jwtToken) => {
		setToken(jwtToken);

		localStorage.setItem(storageName, JSON.stringify({token: jwtToken}));		
	}, [])

	const logout = useCallback(() => {
		setToken(null);
		setUser(null);
		localStorage.removeItem(storageName);
	}, []);

	useEffect(() => {
		const data = JSON.parse(localStorage.getItem(storageName))
		if(data && data.token) {
			login(data.token)
		}
	}, [login]);
	
	const me = useCallback((user) => {
		setUser(user);
	}, [setUser])

	const userDataa = useCallback(async() => {
		try{
			const data = await request(USER, 'GET')
			await me(data)
		} catch {

		}finally {
      		setIsLoaded(true);
    	}	
	}, [me, request])

	useEffect(() => {
		userDataa()
	}, [userDataa])

	const getAllTask = useCallback((tasks) => {
		setTasks(tasks)
	}, [])

	const contextValue = useMemo(
	() => ({
		login,
		logout,
		token,
		user,
		task,
		tasks,
		avatar,
		me,
		setTask,
		setAvatar,
		getAllTask,
		isLoaded
	}),
		[login, logout, token, user, me, isLoaded, avatar, setAvatar, 
		getAllTask, tasks, setTask, task]
	);

  	return (
	    <AuthContext.Provider value={contextValue}>
	      {props.children}
	    </AuthContext.Provider>
	);
}