import React from 'react';
import {Routes, Route} from 'react-router-dom';
import LoginPage from './../components/Login/LoginPage';
import SignUpPage from './../components/SignUp/SignUpPage';
import ProfilePage from './../components/Profile/ProfilePage';
import UpdateAge from './../components/Profile/UpdateProfile/UpdateAge';
import UploadImage from './../components/Profile/UpdateProfile/UploadImage';
import Dashboard from './../components/Dashboard/Dashboard';
import ThemePage from './../components/Theme/ThemePage';
import ToDoList from './../components/ToDoList/ToDoList';
import CreateTask from './../components/ToDoList/GreateTask/GreateTask';
import CompletedTask from './../components/ToDoList/CompletedTask/CompletedTask';
import Task from './../components/ToDoList/Task/Task';
import StatusOk from './../components/common/Status/StatusOk';
import StatusError from './../components/common/Status/StatusError';
import PrivateRoute from './PrivateRoute';
import GuestRoute from './GuestRoute';
import useAuthContext from '../hooks/useAuthContext';

export const ToDoRoutes = ({handleDarkThemeClick, handleBlueThemeClick, handleDarkGreenThemeClick}) => {
	const auth = useAuthContext();
	return auth.isLoaded ? ( 
		<Routes>
			<Route path='/' element={<Dashboard />}/>
			<Route
		        path="profile"
		        element={
		          <PrivateRoute>
		            <ProfilePage />
		          </PrivateRoute>
		        }
			/>
			<Route
		        path="update"
		        element={
		          <PrivateRoute>
		            <UpdateAge />
		          </PrivateRoute>
		        }
			/>
			<Route
		        path="avatar"
		        element={
		          <PrivateRoute>
		            <UploadImage />
		          </PrivateRoute>
		        }
			/>
			<Route
		        path="todolist"
		        element={
		          <PrivateRoute>
		            <ToDoList />
		          </PrivateRoute>
		        }
			/>
			<Route
		        path="greate_task"
		        element={
		          <PrivateRoute>
		            <CreateTask />
		          </PrivateRoute>
		        }
			/>
			<Route
		        path="task/:id"
		        element={
		          <PrivateRoute>
		            <Task />
		          </PrivateRoute>
		        }
			/>
			<Route
		        path="completed_tasks"
		        element={
		          <PrivateRoute>
		            <CompletedTask />
		          </PrivateRoute>
		        }
			/>
			<Route
		        path="theme"
		        element={
		          <PrivateRoute>
		            <ThemePage 
		            	handleDarkThemeClick={handleDarkThemeClick} 
		            	handleBlueThemeClick={handleBlueThemeClick}
		            	handleDarkGreenThemeClick={handleDarkGreenThemeClick}
	            	/>
		          </PrivateRoute>
		        }
			/>
			<Route
		        path="login"
		        element={
		          <GuestRoute>
		            <LoginPage />
		          </GuestRoute>
		        }
			/>
			<Route
		        path="sign_up"
		        element={
		          <GuestRoute>
		            <SignUpPage />
		          </GuestRoute>
		        }
			/>
			<Route path='success' element={<StatusOk />}/>
			<Route path='errors' element={<StatusError />}/>
		</Routes>
	) : null
}
