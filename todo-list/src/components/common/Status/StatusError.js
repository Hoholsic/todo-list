import React from 'react';
import {Link} from 'react-router-dom';
import style from './StatusError.module.css';
import iconError from '../../../image/iconError.png';

const StatusError = (props) => {
	return (
		<div className={style.status}>
			<div className={style.window}>
				<div className={style.item}>
					<img src={iconError} alt="error" />
					<h2 className={style.message}>error</h2>
				</div>
				<p className={style.text}>user with such data has already been created</p>
				<Link to="/sign_up" className = {style.link}>back</Link>
			</div>	
		</div>	
	)
}

export default StatusError