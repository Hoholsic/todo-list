import React from 'react';
import {Link} from 'react-router-dom';
import style from './StatusOk.module.css';
import iconOk from '../../../image/iconOk.png';

const StatusOk = (props) => {
	return (
		<div className={style.status}>
			<div className={style.window}>
				<div className={style.item}>
					<img src={iconOk} alt="ok" />
					<h2 className={style.message}>success</h2>
				</div>
				<p className={style.text}>Congratulations, your account has been successfuly created</p>
				<Link to="/login" className = {style.link}>contiue</Link>
			</div>			
		</div>
	)
}

export default StatusOk