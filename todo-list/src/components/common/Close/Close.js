import React from 'react';
import {useNavigate} from 'react-router-dom';
import style from './Close.module.css';

const Close = (props) => {
	const navigate = useNavigate();

	const closeHendler = () => {
		navigate(-1);
	}

	return (
		<>
		    <div onClick={closeHendler} className={style.close}>
		    	<span className={style.line}></span>
		    	<span className={style.line}></span>
		    </div>
			
		</>
	)
}

export default Close