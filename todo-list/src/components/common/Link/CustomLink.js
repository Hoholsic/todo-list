import React from 'react';
import {Link} from 'react-router-dom';
import style from './CustomLink.module.css';

const CustomLink = ({path, text}) => {
	return (
		<div className={style.btn}>
			<Link to={path} className={style.title}>{text}</Link>					
		</div>
	)
}
export default CustomLink;