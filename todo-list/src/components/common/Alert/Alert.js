import React, { useState } from "react";
import style from "./Alert.module.css";
import css from "classnames";

export default function Alert ({ children, type, message, clearError }) {
  const [isShow, setIsShow] = useState(true)

  const renderElAlert = function () {
      return React.cloneElement(children);
    };

  const handleClose = (e) => {
    e.preventDefault();
    setIsShow(false);
    clearError();
  };

  return (
    <div className={css(style.alert, style[type], !isShow ? style.hide : style.show)}>
      <span className={style.closebtn} onClick={handleClose}>
        &times;
      </span>
      {children ? renderElAlert() : message}
    </div>
  );
}