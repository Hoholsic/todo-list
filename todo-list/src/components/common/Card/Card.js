import React from 'react';
import {Link} from 'react-router-dom';
import style from './Card.module.css';


const Card = ({path, icon, title, text}) => {
	return (
		<Link 
			to={path}
			className={style.card}
		>
			<div className={style.header}>
				<img src={icon} alt="icon" />
			</div>
			<h2 className={style.headline}>{title}</h2>
			<p className={style.subtitle}>{text}</p>
		</Link>
	)
};

export default Card;