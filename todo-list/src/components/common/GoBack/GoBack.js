import React from 'react';
import {useNavigate} from 'react-router-dom';
import style from './GoBack.module.css';

const GoBack = (props) => {
	const navigate = useNavigate();

	const goBackHendler = () => {
		navigate(-1);
	}

	return (
		<div className={style.back} onClick={goBackHendler}>
			<span className={style.text}>Go Back</span>		
		</div>
	)
}

export default GoBack