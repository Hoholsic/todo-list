import React from 'react';
import style from './Preloader.module.css';
import preloader from '../../../image/preloader.gif';

const Preloader = (props) => {
	return (
		<div className={style.preloader}>
			<img src={preloader} alt="Loading..." className={style.load} />
		</div>
	)
}

export default Preloader