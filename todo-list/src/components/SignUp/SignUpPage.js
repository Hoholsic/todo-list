import React from 'react';
import style from './SignUpPage.module.css';
import {Link, useNavigate } from 'react-router-dom';
import { useForm } from "react-hook-form";
import iconPassword from '../../image/iconPassword.png';
import iconEmail from '../../image/iconEmail.png';
import iconAge from '../../image/iconAge.png';
import iconName from '../../image/iconName.png';
import {useHttp} from '../../hooks/http.hook';
import {REGISTRATION} from '../../api/endpoints';
import Preloader from '../common/Preloader/Preloader';

const SignUpPage = (props) => {
	const {isLoading, request} = useHttp();
	let navigate = useNavigate();
	const { 
		register,
		formState: {errors, isValid},
		handleSubmit,
		reset
	} = useForm({
		mode: "all"
	});

	const registerHendler = async(dataForm) => {
		try{
			await request(REGISTRATION, 'POST', dataForm)	
			navigate("../success", { replace: true });
		} catch (error) {
			navigate("../errors", { replace: true });
		}
		reset();
	}

	return (
		<>
		{isLoading ? <Preloader /> : null}			
			<div className = {style.wrapper}>
			 	<form onSubmit={handleSubmit(registerHendler)} className = {style.form}>
			 		<h1 className = {style.title}>Registration</h1>
			 		<p className = {style.text}>
			 	    	Already have an account? 
			 	    	<Link to="/login" className = {style.link}>Sign In</Link>
			 	    </p>
			 		<label className = {style.label}>
			 		<img src={iconName} alt="name" className = {style.icon}/>First Name:
						<input className = {style.input}
							{...register("name", {
								required: "The field is required to be filled",
							})} 
						/>	
			 		</label>
			 		<div className = {style.error}>
			 			{errors?.firstName && <p>{errors?.firstName?.message || "Error"}</p>}
			 		</div>
			 		<label className = {style.label}> 
			 		<img src={iconEmail} alt="email" className = {style.icon}/>Email:
						<input className = {style.input}
							{...register("email", {
								required: "the field is required to be filled",
							})} 
						/>	
			 		</label>
			 		<div className = {style.error}>
			 			{errors?.email && <p>{errors?.email?.message || "Error"}</p>}
			 		</div>
			 		<label className = {style.label}>
			 		<img src={iconPassword} alt="password" className = {style.icon}/> Password:
						<input className = {style.input}
							{...register("password", {
								required: "the field is required to be filled",
								minLength: {
									value: 8,
									message: "numbness 8 symbol"
								},
								pattern: {
								  value: /^[A-Za-z0-9]+$/i,
								  message: "password can only have latin letters and numbers"
								}
							})} 
						/>	
			 		</label>
			 		<div className = {style.error}>
			 			{errors?.password && <p>{errors?.password?.message || "Error"}</p>}
			 		</div>
			 		<label className = {style.label}> 
			 		<img src={iconAge} alt="age" className = {style.icon}/>Age:
						<input className = {style.input}
							{...register("age", {
								required: "the field is required to be filled",
								pattern: {
								  value: /^[0-95]+$/i,
								  message: "age can only have latin numbers"
								},
								pattern: {
								  value: /^\d+$/,
								  message: "age can only have integer"
								}
							})} 
						/>	
			 		</label>
			 		<div className = {style.error}>
			 			{errors?.age && <p>{errors?.age?.message || "Error"}</p>}
			 		</div>
			 	    <input 
			 	    	type="submit" 
			 	    	className = {style.btn}
			 	    	disabled = {!isValid}
		 	    	/>
		 	    </form>
			</div>
		</>
	)
}

export default SignUpPage;
