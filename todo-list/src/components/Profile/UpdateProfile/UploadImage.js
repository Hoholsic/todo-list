import React, { useState, useCallback } from 'react';
import { useForm } from "react-hook-form";
import {useNavigate} from 'react-router-dom';
import {AVATAR} from '../../../api/endpoints';
import style from './UploadImage.module.css';
import Preloader from '../../common/Preloader/Preloader';
import GoBack from '../../common/GoBack/GoBack';
import iconUsers from '../../../image/iconUsers.png';
import Close from '../../common/Close/Close';

const UploadImage = (props) => {
	const storageName = 'userData';
	const navigate = useNavigate();
	const [isLoading, setIsLoading] = useState(false);
	const [nameImg, setNameImg] = useState(null);
	const [image, setImage] = useState(null);

	const { register,
		formState: {isValid},
		handleSubmit,
		reset
	} = useForm({
		mode: "all"
	});

	const previewImage = useCallback((event) => {
		event.preventDefault();
		   if (event.target.files[0]) {
		   	   const name = event.target.files[0].name
		       setNameImg(name)
		       const img = URL.createObjectURL(event.target.files[0])
		       setImage(img)
		   }
	}, [setNameImg, setImage])

	const avatarHendler = useCallback(async(data) => {
		setIsLoading(true)
		try{
			const formData = new FormData()
			formData.append("avatar", data.avatar[0])
			const options = {
			    method: "POST",
			    headers: {'Accept': 'application/json'},
			    body: formData
			}
			if(options.headers) {
				const dataToken = JSON.parse(localStorage.getItem(storageName));
				if (dataToken && dataToken.token) {
			    	const token = dataToken.token
			      	options.headers.authorization = `Bearer ${token}`;
			    }
			}
			const response = await fetch(AVATAR, options);
			const dataAvatar = await response.json(); 
			navigate(-1);
			if(!response.ok) {
				throw new Error(dataAvatar.error || 'some error has occurred')
			}
			setIsLoading(false)
		} catch (error) {
			setIsLoading(false)
		}
		reset();
	}, [navigate, reset])

	const closeImg = () => {
		setNameImg(null)
		setImage(null)
	}

	return (
		<>
			{isLoading ? <Preloader /> : null}
			<div className={style.avatar}>
				<GoBack />
				<div className={style.inner}>
				    <h1 className = {style.title}>Upload Image</h1>
				    <Close />
				 	<form onSubmit={handleSubmit(avatarHendler)} className = {style.form}>	
						<label className={style.label} htmlFor="file">{nameImg ? nameImg : "upload image"}</label>
						<input className = {style.input}
							id="file"
							type="file" 
							{...register("avatar")}  
							accept=".jpg, .jpeg, .png"
							onChange={previewImage}
						/>
						<div className={style.img}>
							<img src={image ? image : iconUsers} alt="icon" />
							<div onClick={closeImg} className={style.close}>
						    	<span className={style.line}></span>
						    	<span className={style.line}></span>
						    </div>
						</div>
				 	    <input 
				 	    	type="submit" 
				 	    	className = {style.button}
				 	    	disabled = {!isValid}
			 	    	/>
			 	    </form>
				</div>
			</div>
		</>
	)
}

export default UploadImage