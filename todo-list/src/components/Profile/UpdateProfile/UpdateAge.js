import React from 'react';
import { useForm } from "react-hook-form";
import {useNavigate} from 'react-router-dom';
import useAuthContext from '../../../hooks/useAuthContext';
import {useHttp} from '../../../hooks/http.hook';
import {USER} from '../../../api/endpoints';
import style from './UpdateProfile.module.css';
import Preloader from '../../common/Preloader/Preloader';
import GoBack from '../../common/GoBack/GoBack';
import Close from '../../common/Close/Close';
import Alert from '../../common/Alert/Alert';

const UpdateAge = (props) => {
	const {isLoading, request, error, clearError } = useHttp();
	const navigate = useNavigate();
	const auth = useAuthContext();

	const { 
		register,
		formState: {isValid},
		handleSubmit,
		reset
	} = useForm({
		mode: "all"
	});

	const updateAgeHendler = async(dataForm) => {
		try{
			const data = await request(USER, 'PUT', dataForm)
			auth.me(data.data)
			navigate(-1);
		} catch (error) {}
		reset();
	}

	return (
		<>
			{isLoading ? <Preloader /> : null}
			<div className={style.update}>
				<GoBack />
				{error ? <Alert type="error" message={error} clearError={clearError}/> : null}
				<div className={style.inner}>
				    <h1 className = {style.title}>Update Profile</h1>
				    <Close />
					<form onSubmit={handleSubmit(updateAgeHendler)} className = {style.form}>
				 		<label className = {style.label}> age
							<input className = {style.input}
								{...register("age", {
									required: "the field is required to be filled"
								})} 
							/>	
				 		</label>
				 	    <button 
				 	    	type="submit" 
				 	    	className = {style.btn}
				 	    	disabled = {!isValid}
			 	    	>
			 	    		SAVE
			 	    	</button>	
			 	    </form>
				</div>
			</div>
		</>
	)
}

export default React.memo(UpdateAge)