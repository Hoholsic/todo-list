import React from 'react';
import style from './Avatar.module.css';
import iconUsers from '../../../image/iconUsers.png';
import useAuthContext from '../../../hooks/useAuthContext';

const Avatar = (props) => {
	const auth = useAuthContext();
	
	return(
		<div className={style.avatar}>
			<img src={auth.avatar ? auth.avatar : iconUsers} alt="icon" className={style.img} />
		</div>
	)
}

export default Avatar;