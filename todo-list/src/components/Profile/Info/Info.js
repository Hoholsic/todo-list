import React from 'react';
import style from './Info.module.css';

const Info = ({user}) => {
	return(
		<div className={style.info}>
			<div className={style.name}>
				{user.name}
			</div>
			<div className={style.age}>
				{user.age} age
			</div>
			<div className={style.email}>
				{user.email}
			</div>
		</div>
	)
}

export default React.memo(Info);