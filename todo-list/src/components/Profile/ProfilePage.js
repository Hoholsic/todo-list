import React, { useCallback, useEffect } from 'react';
import style from './ProfilePage.module.css';
import {Link, useNavigate} from 'react-router-dom';
import {useHttp} from '../../hooks/http.hook';
import {USER, AVATAR} from '../../api/endpoints';
import Avatar from './Avatar/Avatar';
import Info from './Info/Info';
import GoBack from '../common/GoBack/GoBack';
import useAuthContext from '../../hooks/useAuthContext';

const ProfilePage = (props) => {
	const auth = useAuthContext();
	const {request} = useHttp();
	const navigate = useNavigate();

	const getAvatar = useCallback(async() => {
		try{
			const res = await fetch(`https://api-nodejs-todolist.herokuapp.com/user/${auth.user._id}/avatar`, {method: 'GET'})
		    const avatarBlob = await res.blob();
		    if(!res.ok) {
				throw new Error(avatarBlob.error || 'some error has occurred')
			}
		    const avatarURL = URL.createObjectURL(avatarBlob);
			auth.setAvatar(avatarURL);
		} catch (error) {
		}
	}, [auth.setAvatar])

	useEffect(() => {
		getAvatar()
	}, [getAvatar])

	const deleteUser = useCallback(async() => {
		try{
			await request(USER, 'DELETE', {})
			auth.me(null);
			navigate("/sign_up")
		} catch (error) {}	
	}, [request, navigate])

	const deleteAvatar = useCallback(async() => {
		try{
			await request(AVATAR, 'DELETE', {})
			auth.setAvatar(null);
		} catch (error) {
		}	
	}, [request])

	return (
		<>
			<div className = {style.profile}>
				<GoBack />
				<Avatar />
				<div className={style.body}>
					<Info user={auth.user} />
					<div className={style.settings}>
						<Link to='/update' className={style.btn}>Update Profile</Link>	
						<div className={style.btn} onClick={deleteUser}>Delete User</div>
						<Link to='/avatar' className={style.btn}>Update Avatar</Link>
						<div className={style.btn} onClick={deleteAvatar}>Delete Avatar</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default ProfilePage