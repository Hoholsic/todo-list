import React, { useCallback, useEffect } from 'react';
import {useNavigate, useParams} from 'react-router-dom';
import style from './Task.module.css';
import {useHttp} from '../../../hooks/http.hook';
import Preloader from '../../common/Preloader/Preloader';
import useAuthContext from '../../../hooks/useAuthContext';
import GoBack from '../../common/GoBack/GoBack';
import Close from '../../common/Close/Close';
import Alert from '../../common/Alert/Alert';
import iconDelete from '../../../image/iconDelete.png';

const Task = (props) => {
	const {isLoading, request, error, clearError } = useHttp();
	const auth = useAuthContext();
	let navigate = useNavigate();
	let {id} = useParams()

	const getTask = useCallback(async() => {
		const data = await request(`https://api-nodejs-todolist.herokuapp.com/task/${id}`, 'GET')
		auth.setTask(data.data)
	}, [request, id])
	useEffect(() => {
		getTask();
	}, [getTask])

	const deleteTask = useCallback(async() => {
		await request (`https://api-nodejs-todolist.herokuapp.com/task/${id}`, 'DELETE', {})
		navigate(-1);
	}, [request, id, navigate])

	return (
		<>
			{isLoading ? <Preloader /> : null}
			<div className={style.wrapper}>	
				<GoBack />	
				{error ? <Alert type="error" message={error} clearError={clearError}/> : null}
				<div className={style.inner}>
					<h1 className = {style.title}>Task</h1>
					<Close />
					{auth.task
						?(<div className={style.body}>
							<div className={style.data}>
								{auth.task.createdAt.slice(0,10) + ' / ' + auth.task.createdAt.slice(11, -5)}
							</div>
							<div className={style.item}>
								<div className={style.text}>
									{auth.task.description}	
								</div>
								<div className={style.btn} onClick={deleteTask}>
									<img src={iconDelete} alt="icon" />
								</div>
							</div>
						</div>): null}
				</div>	
			</div>
		</>
	)
}
export default Task;