import React, { useCallback, useEffect } from 'react';
import {Link} from 'react-router-dom';
import style from './CompletedTask.module.css';
import {COMPLETED} from '../../../api/endpoints';
import {useHttp} from '../../../hooks/http.hook';
import Preloader from '../../common/Preloader/Preloader';
import useAuthContext from '../../../hooks/useAuthContext';
import GoBack from '../../common/GoBack/GoBack';
import Alert from '../../common/Alert/Alert';
import iconDelete from '../../../image/iconDelete.png';
import todo from '../../../image/todo.gif';
import circle from '../../../image/circle.png';
import complete from '../../../image/completed.png';

const CompletedTask = (props) => {
	const {isLoading, request, error, clearError } = useHttp();
	const auth = useAuthContext();

	const getAllTask = useCallback(async() => {
		const data = await request(COMPLETED, 'GET')
		auth.getAllTask(data)
	}, [request])

	useEffect(() => {
		getAllTask();
	}, [getAllTask])

	const deleteTask = useCallback(async(id) => {
		await request (`https://api-nodejs-todolist.herokuapp.com/task/${id}`, 'DELETE', {})
		const data = await request(COMPLETED, 'GET')
		auth.getAllTask(data)
	}, [request])

	return (
		<>
			{isLoading ? <Preloader /> : null}
			<div className={style.wrapper}>	
			<GoBack />	
			{error ? <Alert type="error" message={error} clearError={clearError}/> : null}
	 	    	<div className={style.items}>
	 	    		{auth.tasks.count > 0 ? ( auth.tasks.data
	 	    			.sort((a, b) => a.createdAt > b.createdAt ? 1 : -1)
	 	    			.reverse()
	 	    			.map(({completed, description, _id}) => <div key={_id} className={style.item} >
 	    					<button 
					 	    	type="submit" 
					 	    	className={style.completed}
					 	    	disabled = {completed === true}
				 	    	>
 	    						<img src={completed === true ? complete : circle} alt="icon" />
				 	    	</button>
				 	    	{completed === true
				 	    		? <p className={style.text}>{description}</p>			
		 	    				: <Link to={`/task/${_id}`}>{description}</Link>
				 	    	}
	 	    				<div onClick={() => {deleteTask(_id)}} className={style.btn_delete}>
	 	    					<img src={iconDelete} alt="icon" />
 	    					</div>
	 	    			</div>)) : (
	 	    			<div className={style.gif}>
	 	    				<p className={style.title}>all completed tasks</p>
	 	    				<img src={todo} alt="gif" />
	 	    			</div>
	 	    		)}
	 	    	</div>
			</div>
		</>
	)
}
export default CompletedTask;