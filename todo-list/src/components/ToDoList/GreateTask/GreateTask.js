import React from 'react';
import { useForm } from "react-hook-form";
import {useNavigate} from 'react-router-dom';
import style from './GreateTask.module.css';
import {TASK} from '../../../api/endpoints';
import {useHttp} from '../../../hooks/http.hook';
import Preloader from '../../common/Preloader/Preloader';
import GoBack from '../../common/GoBack/GoBack';
import Close from '../../common/Close/Close';
import Alert from '../../common/Alert/Alert';

const CreateTask = (props) => {
	const {isLoading, request, error, clearError } = useHttp();
	let navigate = useNavigate();

	const { 
		register,
		formState: {isValid},
		handleSubmit,
		reset
	} = useForm({
		mode: "all"
	});

	const addTask = async(dataForm) => {
		try{
			await request(TASK, 'POST', dataForm)
			navigate(-1);
		} catch (error) {}
		reset();
	}

	return (
		<>
			{isLoading ? <Preloader /> : null}
			<div className={style.wrapper}>
				<GoBack />
				{error ? <Alert type="error" message={error} clearError={clearError}/> : null}
				<div className={style.inner}>
					<h1 className = {style.title}>Greate Task</h1>
					<Close />
					<form onSubmit={handleSubmit(addTask)} className = {style.form}>
				 		<label className = {style.label}>task
				 		<textarea className = {style.input}
				 			{...register("description", {
									required: "the field is required to be filled"
								})} >
				 		</textarea>
				 		</label>
				 	    <button 
				 	    	type="submit" 
				 	    	className = {style.btn}
				 	    	disabled = {!isValid}
			 	    	>
			 	    		SAVE
			 	    	</button>
			 	    </form>
		 	    </div>
			</div>
		</>
	)
}
export default CreateTask;