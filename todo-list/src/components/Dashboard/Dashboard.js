import React from 'react';
import style from './Dashboard.module.css';
import iconUsers from '../../image/iconUsers.png'
import iconChecklist from '../../image/iconChecklist.png'
import iconTheme from '../../image/iconTheme.png';
import Card from '../common/Card/Card';


const Dashboard = (props) => {
	return (
		<div className = {style.main}>
			<div className = {style.info}>
				<h1 className= {style.title}>plan your life wisely</h1>
				<p className = {style.text}>Create clear, multi-functional to-do lists to easily manage your ideas and work from anywhere so you never forget anything again.</p>
			</div>
			<Card path={'/profile'}
				icon={iconUsers}
				title={'profile'}
				text={'Customize your profile the way You want'}
			/>
			<Card path={'/todolist'}
				icon={iconChecklist}
				title={'to-do list'}
				text={'Create your own to-do list, which you can change later'}
			/>
			<Card path={'/theme'}
				icon={iconTheme}
				title={'theme'}
				text={'Choose a theme that you like and will be comfortable for further work'}
			/>
		</div>
	)
};

export default Dashboard;