import React, { useCallback, useEffect } from 'react';
import style from './Header.module.css';
import {Link} from 'react-router-dom';
import useAuthContext from '../../hooks/useAuthContext';
import {useHttp} from '../../hooks/http.hook';
import {LOGOUT} from '../../api/endpoints';
import iconUsers from '../../image/iconUsers.png';

const Header = ({user}) => {
	const { request } = useHttp();
	const auth = useAuthContext();
	const logoutHendler = async() => {
		try{
			await request(LOGOUT, 'POST', {});
			auth.logout();
		} catch (error) {}	
	}
	
	const getAvatar = useCallback(async() => { 
		try{
			const id = localStorage.getItem('user');
			const res = await fetch(`https://api-nodejs-todolist.herokuapp.com/user/${id}/avatar`, {method: 'GET'})
		    const avatarBlob = await res.blob();
		    if(!res.ok) {
				throw new Error(avatarBlob.error || 'some error has occurred')
			}
		    const avatarURL = URL.createObjectURL(avatarBlob);
			auth.setAvatar(avatarURL);
		} catch (error) {
			console.log(error)
		}
	}, [auth.setAvatar])

	useEffect(() => {
		getAvatar();
	}, [getAvatar])

	return (
		<nav className = {style.header}>
			<h1 className = {style.logo}>To do list</h1>
			{auth.token && auth.user
				? (<div className={style.block}>
						<Link to="/profile" className ={style.user} >
							<img src={auth.avatar ? auth.avatar : iconUsers} alt="icon" className={style.avatar} />
							<span className ={style.name}>{auth.user.name}</span>
						</Link>	
						<button 
						onClick={logoutHendler}
						className={style.logout}
						>
							LOG OUT
					  </button>
				</div>)
				: (<div>
					<Link 
						to="/login" 
						className ={style.btn}>
						LOGIN
					</Link>	
					<Link 
						to="/sign_up" 
						className ={style.btn}>
						REGISTRATION
					</Link>		
				</div>)
			}		
	 	</nav>
	)
}
export default Header;