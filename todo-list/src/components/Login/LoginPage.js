import React from 'react';
import style from './LoginPage.module.css';
import { useForm } from "react-hook-form";
import {Link} from 'react-router-dom';
import iconPassword from '../../image/iconPassword.png';
import iconEmail from '../../image/iconEmail.png';
import {useHttp} from '../../hooks/http.hook';
import {LOGIN} from '../../api/endpoints';
import useAuthContext from '../../hooks/useAuthContext';
import Preloader from '../common/Preloader/Preloader';
import Alert from '../common/Alert/Alert';

const LoginPage = (props) => {
	const {error, clearError, isLoading, request} = useHttp();
	const auth = useAuthContext();

	const { 
		register,
		formState: {errors, isValid},
		handleSubmit,
		reset
	} = useForm({
		mode: "all"
	});

	const loginHendler = async(dataForm) => {
		try{
			const data = await request(LOGIN, 'POST', dataForm)
			auth.login(data.token);
			auth.me(data.user);
			localStorage.setItem('user', data.user._id)
		} catch (error) {}
		reset();
	}
	return (
		<>
		{isLoading ? <Preloader /> : null}
		<div className = {style.wrapper}>
			{error ? <Alert type="error" message={error} clearError={clearError}/> : null}
		 	<form onSubmit={handleSubmit(loginHendler)} className = {style.form}>
		 		<h1 className = {style.title}>Sign In</h1>
		 		<p className = {style.text}>
			 	    	If you do not have an account? 
			 	    	<Link to="/sign_up" className = {style.link}>Sing Up</Link>
			 	    </p>
		 		<label className = {style.label}> 
		 		<img src={iconEmail} alt="email" className = {style.icon}/>Email: 
					<input className = {style.input}
						{...register("email", {
							required: "the field is required to be filled",
						})} 
					/>	
		 		</label>
		 		<div className = {style.error}>
		 			{errors?.email && <p>{errors?.email?.message || "Error"}</p>}
		 		</div>
		 		<label className = {style.label}> 
		 		<img src={iconPassword} alt="password" className = {style.icon}/>Password:
					<input className = {style.input}
						{...register("password", {
							required: "the field is required to be filled",
							minLength: {
								value: 8,
								message: "numbness 8 symbol"
							},
							pattern: {
							  value: /^[A-Za-z0-9]+$/i,
							  message: "password can only have latin letters and numbers"
							}
						})} 
					/>	
		 		</label>
		 		<div className = {style.error}>
		 			{errors?.password && <p>{errors?.password?.message || "Error"}</p>}
		 		</div>
		 	    <input 
		 	    	type="submit" 
		 	    	className = {style.btn}
		 	    	disabled = {!isValid}
	 	    	/>
	 	    </form>
		</div>
		</>
	)
}

export default LoginPage;