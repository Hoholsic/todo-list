import React from 'react';
import style from './ThemePage.module.css';
import GoBack from '../common/GoBack/GoBack';

const ThemePage = ({handleDarkThemeClick, handleBlueThemeClick, handleDarkGreenThemeClick}) => {
	return (
		<div className={style.wrapper}>
			<GoBack />
			<h1 className= {style.title}>Theme</h1>
			<button className={style.theme} onClick={handleDarkThemeClick}>
				dark
			</button>
			<button className={style.theme} onClick={handleBlueThemeClick}>
				blue
			</button>
			<button className={style.theme} onClick={handleDarkGreenThemeClick}>
				dark green
			</button>
		</div>
	)
}
export default ThemePage;