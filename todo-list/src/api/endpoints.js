export const REGISTRATION = 'https://api-nodejs-todolist.herokuapp.com/user/register';
export const LOGIN = 'https://api-nodejs-todolist.herokuapp.com/user/login';
export const LOGOUT = 'https://api-nodejs-todolist.herokuapp.com/user/logout';
export const USER = 'https://api-nodejs-todolist.herokuapp.com/user/me';
export const AVATAR= 'https://api-nodejs-todolist.herokuapp.com/user/me/avatar';
export const TASK= 'https://api-nodejs-todolist.herokuapp.com/task';
export const COMPLETED = 'https://api-nodejs-todolist.herokuapp.com/task?completed=true';
