import React from 'react';
import './App.css';
import {ToDoRoutes} from './routes/routes';
import Header from './components/Header/Header';
import {useTheme} from './hooks/useTheme';

const App = (props) => {
  const {theme, setTheme} = useTheme();

  const handleDarkThemeClick = () => {
    setTheme('darkTheme')
  }
  const handleBlueThemeClick = () => {
    setTheme('blueTheme')
  }
  const handleDarkGreenThemeClick = () => {
    setTheme('darkGreenTheme')
  }
  return ( 
    <div className="wrapper">
      <Header />
      <ToDoRoutes 
        handleDarkThemeClick={handleDarkThemeClick} 
        handleBlueThemeClick={handleBlueThemeClick}
        handleDarkGreenThemeClick={handleDarkGreenThemeClick}
      />
    </div>
  );
}
export default App;

