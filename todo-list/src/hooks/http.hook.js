import { useState, useCallback} from 'react';

const storageName = 'userData';

export const useHttp = () => {
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState(null);

	const request = useCallback( async(url, method = 'GET', body = null, headers = {}) => {
		setIsLoading(true)
		try{
			const options = {
			    method: method,
			    mode:'cors',
			    headers: {
			        'Content-Type': 'application/json'  
			    }
			}
			if(options.headers) {
				const dataToken = JSON.parse(localStorage.getItem(storageName));
				if (dataToken && dataToken.token) {
			    	const token = dataToken.token
			      	options.headers.authorization = `Bearer ${token}`;
			    }else{
			    	options.headers.authorization = `Bearer ${null}`;
			    }
			}
			if (['POST', 'PUT', 'DELETE'].includes(method)) {
		        options.body = JSON.stringify(body);
		    }
			const response = await fetch(url, options)
			const data = await response.json()
			if(!response.ok) {
				throw new Error(data || 'some error has occurred')
			}
			setIsLoading(false)
			return data
		} catch(error){
			setIsLoading(false);
			setError(error.message);
			throw error
		}
	}, [error])


	const clearError = useCallback(() => setError(null), [setError]);

	return { isLoading, request, error, clearError }
}